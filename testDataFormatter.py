'''
Created on Jan 23, 2015

@author: Ashton Mendes
'''

import sys
import os

def getFeatures(rawFeature):
    rawFeature = rawFeature.lower()
    
    punctuations = ['~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '{', '}', '|', ':', '"', '<', '>', '?', '`', '-', '=', '[', ']', '\\', ';', '\'', ',', '.', '/']
        
    extractedFeatures = []
    
    feature = ''
    
    for character in rawFeature:
        if(character in punctuations): #found a punctuation mark
            #extractedFeatures.append(character) #remove this line if you want to remove punctuation marks from your token list
            if(len(feature)>0): 
                extractedFeatures.append(feature)
                feature = ''
        else:
            feature += character
            
    if(len(feature)>0):
        extractedFeatures.append(feature)
        
    return extractedFeatures

def getLabel(fileName):
    return fileName.split('.')[0];

def formatTestData(directoryName):        
    try:        
        for fileName in sorted(os.listdir(directoryName)): #traverse through all files in the directory (in alphabetical order)
            inputFile = open(directoryName+"/"+fileName, mode='r',errors='ignore')                                     
                      
            #write the remaining lines into a single line in the output file
            for line in inputFile:
                line = line.strip()
                
                extractedFeatures = getFeatures(line)#getFeatures(line) #getFeatures(line) #[line]
                
                for feature in extractedFeatures:
                    print(feature+" ", sep = "", end = "")
                
            #line ending at the end in output file
            print("\n", sep = "", end = "")
            
            inputFile.close()
    except FileNotFoundError as e:
        print("Directory not found. Error: "+str(e))
    except BaseException as e:
        print("Unexpected Error occurred: "+str(e)+". File in process: "+str(directoryName+"/"+fileName))

def main():    
    numOfArgs = len(sys.argv) - 1; #1st argument is always the source file name.
    if(numOfArgs < 1):
        print("Argument missing. Correct syntax is \n python3 testDataFormatter.py <inputDirectory>")    
    elif(numOfArgs > 1):
        print("Too many arguments. Correct syntax is \n python3 testDataFormatter.py <inputDirectory>")
    else:
        directoryName = sys.argv[1];
        formatTestData(directoryName);

if __name__ == '__main__': main()