'''
Created on Jan 23, 2015

@author: Ashton Mendes
'''
import sys
import math

def classify(testFilepath, modelFilepath):
    try:
        testFile = open(testFilepath, mode='r', errors='ignore')
        modelFile = open(modelFilepath, mode='r')
        
        #reading the model file
        vocabSize = 0
        tokenDictionary = {} #contains key:value pairs where key is a word and value is a dictionary containing count for that word in each class.
        classDictionary = {} #contains key = class name and value = number of samples in training data belonging to that class.
        classTokenCount = {} #contains key = class name and value = number of tokens found in that class. 
        numOfClasses = 0    
        sizeOfTrainingSet = 0 
        orderOfClasses = []  
          
        lineNumber = 0
        for line in modelFile:
            line = line.strip()
            lineNumber = lineNumber + 1 
            
            if(lineNumber == 1): #number of classes
                numOfClasses = int(line)
                
            elif(lineNumber > 1 and lineNumber <= 1+numOfClasses ): #class names
                classInfo = line.split()
                className = classInfo[0]
                classSamples = classInfo[1]
                classTokens = classInfo[2]
                
                orderOfClasses.append(className)
                classDictionary[className] = classSamples
                classTokenCount[className] = classTokens  
                              
            elif(lineNumber == 2+numOfClasses): #vocab size
                vocabSize = int(line)
                
            else:
                tokenInfo = line.split() #word name followed by counts for each class
                tokenName = tokenInfo[0]
                
                tokenDictionary[tokenName] = {}
                for i in range(1, len(tokenInfo)):
                    tokenDictionary[tokenName][orderOfClasses[i-1]] = tokenInfo[i]
        
        #Calculate total number of samples encountered while training (reqd for calculating priors)
        for classSamples in classDictionary.values():
            sizeOfTrainingSet += int(classSamples)
                
        #******************begin calculations and ascertain classification**********************
        
        #initialization
        log_P_class_given_msg = {}
        for className in orderOfClasses:
            log_P_class_given_msg[className] = 0.0
                  
        #priors
        log_priorProb = {}
        for className in orderOfClasses:
            log_priorProb[className] = math.log(float(classDictionary[className])) - math.log(sizeOfTrainingSet) #P(spam) = numOf(spamMsgs) / totalMsgs
        
        for document in testFile:
            
            log_P_msg_given_class = {}
            for className in orderOfClasses:
                log_P_msg_given_class[className] = 0.0
                            
            words = document.split()
                        
            for word in words:                                    
                if(word in tokenDictionary):
                    for className in orderOfClasses:
                        log_P_msg_given_class[className] = log_P_msg_given_class[className] + math.log(float(tokenDictionary[word][className]) + 1) - math.log(float(classTokenCount[className]) + vocabSize + 1) #Add-one smoothing
                else:
                    for className in orderOfClasses:
                        log_P_msg_given_class[className] = log_P_msg_given_class[className] + math.log(1) - math.log(float(classTokenCount[className]) + vocabSize + 1) #Add-one smoothing
                
            for className in orderOfClasses:
                log_P_class_given_msg[className] = log_priorProb[className] + log_P_msg_given_class[className]
             
            #find argmax
            argmax = orderOfClasses[0]
            for className in orderOfClasses:
                if(log_P_class_given_msg[className] > log_P_class_given_msg[argmax]):
                    argmax = className
             
            print("{}".format(argmax))
        
        testFile.close()
        modelFile.close()          
    except FileNotFoundError as e:
        print("Unable to open the file. Error: "+str(e))
    except BaseException as e:
        print("Unexpected error occurred: "+str(e))  
    

def main():
    numOfArgs = len(sys.argv) - 1
    if(numOfArgs < 2):
        print("One or more arguments are missing. The correct syntax is \npython3 nbclassify.py <modelFile> <testFile>")
    elif(numOfArgs > 2):
        print("Too many arguments. The correct syntax is \npython3 nbclassify.py <modelFile> <testFile>")
    else:
        modelFilepath = sys.argv[1] #0th position contains source file name.
        testFilepath = sys.argv[2]
        
        classify(testFilepath, modelFilepath)

if __name__ == '__main__': main()