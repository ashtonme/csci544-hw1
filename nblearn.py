'''
Created on Jan 22, 2015

@author: Ashton Mendes
'''
import sys

def constructModel(trainingFilepath, modelFilepath):
    try:
        trainingFile = open(trainingFilepath, mode='r')
        modelFile = open(modelFilepath, mode='w')
        
        vocabSize = 0
        tokenDictionary = {} #contains key:value pairs where key is a word and value is a dictionary containing count for that word in each class.
        classDictionary = {} #contains key = class name and value = number of samples in training data belonging to that class.
        classTokenCount = {} #contains key = class name and value = number of tokens found in that class.
        
        #Process the training file
        for sample in trainingFile:
            features = sample.split()          
            
            label = features[0]
            count = 0
            
            del features[0]
            
            if(label not in classDictionary): #class label being encountered for the first time.
                classDictionary[label] = 1
                classTokenCount[label] = 0
            else:
                classDictionary[label] += 1 #1 more sample of class 'label' has been seen. update count.            
            
            #now parse the tokens
            for feature in features:  #after separating punctuations   
                if(feature not in tokenDictionary): #token not in existing vocabulary
                    tokenDictionary[feature] = {}
                    
                if(label not in tokenDictionary[feature]): #this token is being seen for the first time under this class label.
                    tokenDictionary[feature][label] = 1
                else:
                    tokenDictionary[feature][label] += 1 #this token has been seen before under this class label. update count.
                 
                classTokenCount[label] += 1
                count += 1

        #find vocabulary size
        vocabSize = len(tokenDictionary.keys())
        
        #Construct model
        #format: 1st line: number of classes, next n lines: names of n classes classMsgs classWords,then vocab size followed by each word in vocabulary
        orderOfClasses = sorted(classDictionary.keys())
        
        #1st line: number of classes
        modelFile.write("{}\n".format(len(classDictionary.keys())))
        
        #next n lines: names of n classes (order in which they will appear while printing word counts) classMsgs classWords 
        for className in orderOfClasses:
            modelFile.write("{} {} {}\n".format(className, classDictionary[className], classTokenCount[className]))
                           
        #vocab size
        modelFile.write("{}\n".format(vocabSize))
        
        #now start printing tokens
        for token in tokenDictionary:
            modelFile.write("{} ".format(token))
            for className in orderOfClasses:
                if(className not in tokenDictionary[token]): #token occurred 0 times under class 'className' in training data                
                    modelFile.write("0 ")  
                else:
                    modelFile.write("{} ".format(tokenDictionary[token][className])) 
            modelFile.write("\n")                                                 
                            
        trainingFile.close()
        modelFile.close()
    except FileNotFoundError as e:
        print("File not found. Error: {}".format(e))
    except BaseException as e:
        print("Unexpected error occurred: {}".format(e))

def main():
    numOfArgs = len(sys.argv) - 1
    if(numOfArgs < 2):
        print("One or more arguments are missing. The correct syntax is \npython3 nblearn.py <trainingFile> <modelFile>")
    elif(numOfArgs > 2):
        print("Too many arguments. The correct syntax is \npython3 nblearn.py <trainingFile> <modelFile>")
    else:
        trainingFilepath = sys.argv[1] #0th position contains source file name.
        modelFilepath = sys.argv[2]
        
        constructModel(trainingFilepath, modelFilepath)

if __name__ == '__main__': main()