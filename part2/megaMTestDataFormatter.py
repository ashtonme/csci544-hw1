'''
Created on Jan 31, 2015

@author: ashton
'''

import sys
import os

def getFeatures(rawFeature):
    rawFeature = rawFeature.lower()
    
    punctuations = ['~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '{', '}', '|', ':', '"', '<', '>', '?', '`', '-', '=', '[', ']', '\\', ';', '\'', ',', '.', '/']
        
    extractedFeatures = []
    
    feature = ''
    
    for character in rawFeature:
        if(character in punctuations): #found a punctuation mark
            #extractedFeatures.append(character) #remove this line if you want to remove punctuation marks from your token list
            if(len(feature)>0): 
                extractedFeatures.append(feature)
                feature = ''
        else:
            feature += character
            
    if(len(feature)>0):
        extractedFeatures.append(feature)
        
    return extractedFeatures

def formatTestData(testDataDirectory):
    try:            
        classes = {} 
        
        #what symbol to use for which class
        try:
            file = open("megaMclassSymbols.txt", mode='r')
        except FileNotFoundError as e:
            print("megaMclassSymbols.txt not found. Did you delete it? Run megaMTrainingDataFormatter.py again. {}".format(e))
            return
        for line in file:
            className = line.split()[0].strip()
            classSymbol = int(line.split()[1].strip())            
            classes[className] = classSymbol
        file.close()      
        
        for fileName in sorted(os.listdir(testDataDirectory)): #traverse through all files in the directory (in no particular order)
            
            inputFile = open(testDataDirectory+"/"+fileName, mode='r', errors='ignore')
                        
            #label = getLabel(fileName)   #gets the label (present in the filename)
                       
            classSymbol = 0 #classes[label] #0
            
            #FORMAT: eg. 3 Subject 1 please 10 signup 3 ...
            tokens = {}                        
            print("{} ".format(classSymbol), end="")
            
            for line in inputFile:
                features = line.split()
                
                for feature in features:
                    extractedFeatures = getFeatures(feature.strip()) #[feature.strip()]
                    
                    for token in extractedFeatures:
                        if(token not in tokens):
                            tokens[token] = 1
                        else:
                            tokens[token] += 1
                      
            #print the line in training set corresponding to this example
            for token in tokens:
                if(token != "#"): #MegaM does not accept # characters  
                    print("{} {} ".format(token, tokens[token]), end="")  
            
            #leave a line after the current file has been represented  
            print()               
            
            inputFile.close()          
    except FileNotFoundError as e:
        print("Directory not found. Error: "+str(e))
    except BaseException as e:
        inputFile.close()
        print("Unexpected Error occurred: "+str(e))
 
def getLabel(fileName):
    return fileName.split('.')[0].strip();           

def main():
    numOfArgs = len(sys.argv) - 1; #1st argument is always the source file name.
    
    if(numOfArgs < 1):
        print("Argument missing. Correct syntax is \n python3 megaMTestDataFormatter.py <testDataDirectory>") 
    if(numOfArgs == 1):
        testDataDirectory = sys.argv[1]
        formatTestData(testDataDirectory)
    if(numOfArgs > 2):
        print("Too many arguments. Correct syntax is \n python3 megaMTestDataFormatter.py <testDataDirectory>")     

if __name__ == '__main__' : main()