'''
Created on Jan 30, 2015

@author: Ashton Mendes
'''
import sys
import os
import time

def getFeatures(rawFeature):
    rawFeature = rawFeature.lower()
    
    punctuations = ['~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '{', '}', '|', ':', '"', '<', '>', '?', '`', '-', '=', '[', ']', '\\', ';', '\'', ',', '.', '/']
        
    extractedFeatures = []
    
    feature = ''
    
    for character in rawFeature:
        if(character in punctuations): #found a punctuation mark
            #extractedFeatures.append(character) #remove this line if you want to remove punctuation marks from your token list
            if(len(feature)>0): 
                extractedFeatures.append(feature)
                feature = ''
        else:
            feature += character
            
    if(len(feature)>0):
        extractedFeatures.append(feature)
        
    return extractedFeatures

def formatTrainingData(directoryName, formattedTrainingSet):    
    try:    
        classes = []
        vocabulary = {}
        vocabularyID = 1
        
        #get class names and if more than 2 classes encountered, exit the program since svm light only supports binary classification.
        for fileName in os.listdir(directoryName):
            label = getLabel(fileName)
            if(label not in classes):
                classes.append(label)
                if(len(classes)>2):
                    print("SVM light is not designed for classification involving more than 2 classes.")
                    return      
                
        #what symbol to use for ham and what for spam
        file = open("svmClassSymbols.txt", mode='w')
        file.write("{} {}\n{} {}".format(classes[0], 1, classes[1], -1))  
        file.close()
           
        trainingFile = open(formattedTrainingSet, mode='w')   
        
        #SAME VOCAB IS GOING TO BE USED TO FORMAT TRAINING DATA
        vocabFile = open("svmVocabulary.txt", mode="w")
        
        for fileName in os.listdir(directoryName): #traverse through all files in the directory (in no particular order)
            
            inputFile = open(directoryName+"/"+fileName, mode='r', errors='ignore')
                        
            label = getLabel(fileName)   #gets the label (present in the filename)
            classSymbol = 1 if label == classes[0] else -1          
            
            #FORMAT: eg. 1 12:233 678:20 ...
            tokens = {}                        
            trainingFile.write("{} ".format(classSymbol))
            
            for line in inputFile:
                features = line.split()
                
                for feature in features:
                    extractedFeatures = getFeatures(feature.strip()) #getFeatures(feature.strip()) #[feature.strip()]
                    
                    for token in extractedFeatures:
                        indexInVocab = -1
                        
                        try:
                            indexInVocab = vocabulary[token]#+1 because svm light does not accept feature number as 0
                        except KeyError:
                            vocabulary[token] = vocabularyID
                            vocabFile.write("{}\n".format(token))
                            vocabularyID += 1
                            indexInVocab = vocabulary[token]
                        
                        try:
                            tokens[indexInVocab] += 1
                        except KeyError:
                            tokens[indexInVocab] = 1
                      
            #print the line in training set corresponding to this example
            for tokenNumber in sorted(tokens.keys()):
                trainingFile.write("{}:{} ".format(tokenNumber, tokens[tokenNumber]))  
            
            #leave a line after the current file has been represented  
            trainingFile.write("\n")               
            
            inputFile.close()    
            
        vocabFile.close()
        trainingFile.close()          
    except FileNotFoundError as e:
        print("Directory not found. Error: "+str(e))
    except BaseException as e:
        trainingFile.close()
        inputFile.close()
        print("Unexpected Error occurred: "+str(e)+". File in process: "+str(directoryName+"/"+fileName))
        
def getLabel(fileName):
    return fileName.split('.')[0];

def main():
    numOfArgs = len(sys.argv) - 1; #1st argument is always the source file name.
    if(numOfArgs < 1):
        print("Argument missing. Correct syntax is \n python3 svmTrainingDataFormatter.py <inputDirectory> <formattedTrainingSet>")    
    elif(numOfArgs > 2):
        print("Too many arguments. Correct syntax is \n python3 svmTrainingDataFormatter.py <inputDirectory> <formattedTrainingSet>")
    else:
        directoryName = sys.argv[1];
        formattedTrainingSet = sys.argv[2]
        formatTrainingData(directoryName, formattedTrainingSet);
    
if __name__ == '__main__': main()