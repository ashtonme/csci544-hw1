'''
Created on Jan 31, 2015

@author: ashton
'''

import sys
import os

def getFeatures(rawFeature):
    rawFeature = rawFeature.lower()
    
    punctuations = ['~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '{', '}', '|', ':', '"', '<', '>', '?', '`', '-', '=', '[', ']', '\\', ';', '\'', ',', '.', '/']
        
    extractedFeatures = []
    
    feature = ''
    
    for character in rawFeature:
        if(character in punctuations): #found a punctuation mark
            #extractedFeatures.append(character) #remove this line if you want to remove punctuation marks from your token list
            if(len(feature)>0): 
                extractedFeatures.append(feature)
                feature = ''
        else:
            feature += character
            
    if(len(feature)>0):
        extractedFeatures.append(feature)
        
    return extractedFeatures

def formatTrainingData(trainingDataDirectory, devDataDirectory = ""):
    try:            
        #*************** FIRST PROCESS TRAINING DATA **********************
        classes = []           
        
        for fileName in os.listdir(trainingDataDirectory): #traverse through all files in the directory (in no particular order)
            
            inputFile = open(trainingDataDirectory+"/"+fileName, mode='r', errors='ignore')
                        
            label = getLabel(fileName)   #gets the label (present in the filename)
            if(label not in classes): classes.append(label)
            
            classSymbol = classes.index(label)
            
            #FORMAT: eg. 3 Subject 1 please 10 signup 3 ...
            tokens = {}                        
            print("{} ".format(classSymbol), end="")
            
            for line in inputFile:
                features = line.split()
                
                for feature in features:
                    extractedFeatures = getFeatures(feature.strip()) #[feature.strip()]
                    
                    for token in extractedFeatures:
                        if(token not in tokens):
                            tokens[token] = 1
                        else:
                            tokens[token] += 1
                      
            #print the line in training set corresponding to this example
            for token in tokens:
                if(token != "#"): #MegaM does not accept # characters  
                    print("{} {} ".format(token, tokens[token]), end="")  
            
            #leave a line after the current file has been represented  
            print()               
            
            inputFile.close()  
            
        #what symbol to use for which class
        file = open("megaMclassSymbols.txt", mode='w')
        for className in classes:
            file.write("{} {}\n".format(className, classes.index(className)))  
        file.close()    
              
        #*************** NOW PROCESS DEV DATA **********************
        if(devDataDirectory == ""):
            return   
        
        #line saying DEV
        print("DEV")        
        
        for fileName in os.listdir(devDataDirectory): #traverse through all files in the directory (in no particular order)
            
            inputFile = open(devDataDirectory+"/"+fileName, mode='r', errors='ignore')
                        
            label = getLabel(fileName)   #gets the label (present in the filename)
            
            classSymbol = classes.index(label)
            
            #FORMAT: eg. 3 Subject 1 please 10 signup 3 ...
            tokens = {}                        
            print("{} ".format(classSymbol), end="")
            
            for line in inputFile:
                features = line.split()
                
                for feature in features:
                    extractedFeatures = getFeatures(feature.strip()) #[feature.strip()]
                    
                    for token in extractedFeatures:
                        if(token not in tokens):
                            tokens[token] = 1
                        else:
                            tokens[token] += 1
                      
            #print the line in training set corresponding to this example
            for token in tokens:
                if(token != "#"): #MegaM does not accept # characters  
                    print("{} {} ".format(token, tokens[token]), end="")  
            
            #leave a line after the current file has been represented  
            print()               
            
            inputFile.close()
             
    except FileNotFoundError as e:
        print("Directory not found. Error: "+str(e))
    except BaseException as e:
        inputFile.close()
        print("Unexpected Error occurred: "+str(e))
 
def getLabel(fileName):
    return fileName.split('.')[0].strip();           

def main():
    numOfArgs = len(sys.argv) - 1; #1st argument is always the source file name.
    
    if(numOfArgs < 1):
        print("Argument missing. Correct syntax is \n python3 megaMTrainingDataFormatter.py <trainingDataDirectory> [options]\noptions include:\n-dev <developmentDataDirectory>") 
    if(numOfArgs == 1): #only trainingDataProvided
        trainingDataDirectory = sys.argv[1]
        formatTrainingData(trainingDataDirectory)
    if(numOfArgs == 2):
        print("Argument missing. Correct syntax is \n python3 megaMTrainingDataFormatter.py <trainingDataDirectory> [options]\noptions include:\n-dev <developmentDataDirectory>")
    if(numOfArgs == 3):
        if(sys.argv[2] != "-dev"):
            print("Malformed command. Correct syntax is \n python3 megaMTrainingDataFormatter.py <trainingDataDirectory> [options]\noptions include:\n-dev <developmentDataDirectory>")
        else:
            trainingDataDirectory = sys.argv[1]
            devDataDirectory = sys.argv[3]
            formatTrainingData(trainingDataDirectory, devDataDirectory)
    if(numOfArgs > 3):
        print("Too many arguments. Correct syntax is \n python3 megaMTrainingDataFormatter.py <trainingDataDirectory> [options]\noptions include:\n-dev <developmentDataDirectory>")

if __name__ == '__main__' : main()