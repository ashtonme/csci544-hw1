'''
Created on Jan 31, 2015

@author: ashton
'''

import sys

def postProcessOutput(megaMOutputFileName):
    try:
        megaMOutput = open(megaMOutputFileName, mode='r')
       
        #what symbol to use for which class
        classSymbols = {}
        try:
            file = open("megaMclassSymbols.txt", mode='r')
        except FileNotFoundError as e:
            print("megaMclassSymbols.txt not found. Did you delete it? Run megaMTrainingDataFormatter.py again. {}".format(e))
            return
        for line in file:
            className = line.split()[0].strip()
            classSymbol = int(line.split()[1].strip())            
            classSymbols[classSymbol] = className
        file.close()      
        
        #convert predictions to class symbols
        for line in megaMOutput:
            prediction = int(line.split()[0].strip())
            print("{}".format(classSymbols[prediction]))
                
        megaMOutput.close()        
    except FileNotFoundError as e:
        print("File not found. Error: "+str(e))
    except BaseException as e:
        print("Unexpected Error occurred: "+str(e))

def main():
    numOfArgs = len(sys.argv) - 1; #1st argument is always the source file name.
    if(numOfArgs < 1):
        print("Argument missing. Correct syntax is \n python3 megaMPostProcessor.py <megaMOutput>")    
    elif(numOfArgs > 1):
        print("Too many arguments. Correct syntax is \n python3 megaMPostProcessor.py <megaMOutput>")
    else:
        megaMOutputFileName = sys.argv[1];
        postProcessOutput(megaMOutputFileName)

if __name__ == '__main__' : main()