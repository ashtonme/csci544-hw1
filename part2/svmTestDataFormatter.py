'''
Created on Jan 30, 2015

@author: Ashton Mendes
'''
import sys
import os
import time

def getFeatures(rawFeature):
    rawFeature = rawFeature.lower()
    
    punctuations = ['~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '+', '{', '}', '|', ':', '"', '<', '>', '?', '`', '-', '=', '[', ']', '\\', ';', '\'', ',', '.', '/']
        
    extractedFeatures = []
    
    feature = ''
    
    for character in rawFeature:
        if(character in punctuations): #found a punctuation mark
            #extractedFeatures.append(character) #remove this line if you want to remove punctuation marks from your token list
            if(len(feature)>0): 
                extractedFeatures.append(feature)
                feature = ''
        else:
            feature += character
            
    if(len(feature)>0):
        extractedFeatures.append(feature)
        
    return extractedFeatures

def formatTestData(directoryName, formattedTrainingSet):    
    try:    
        classSymbols = {}
        vocabulary = {}   
        vocabularyID = 1  
                
        #what symbol to use for ham and what for spam
        try:
            file = open("svmClassSymbols.txt", mode='r')
        except FileNotFoundError as e:
            print("svmClassSymbols.txt not found. Did you delete it? Run svmTrainingDataFormatter.py again. {}".format(e))
            return
        
        lineNumber = 0
        for line in file:
            lineNumber += 1
            
            if(lineNumber > 2):
                print("SVM light is not designed for classification involving more than 2 classSymbols.")
                return
            
            className = line.split()[0].strip()
            classSymbol = line.split()[1].strip()
            
            classSymbols[className] = classSymbol               
           
        
        #fetch vocabulary
        try:
            vocabFile = open("svmVocabulary.txt", mode='r')
        except FileNotFoundError as e:
            print("svmVocabulary.txt not found. Did you delete it? Run svmTrainingDataFormatter.py again. {}".format(e))
            return
        for token in vocabFile:
            vocabulary[token.strip()] = vocabularyID
            vocabularyID += 1
        vocabFile.close()
        
        testFile = open(formattedTrainingSet, mode='w')         
       
        for fileName in sorted(os.listdir(directoryName)): #traverse through all files in the directory (in no particular order)
            
            inputFile = open(directoryName+"/"+fileName, mode='r', errors='ignore')                        
            
            classSymbol = 0 #classSymbols[getLabel(fileName)] #0
            
            #FORMAT: eg. 1 12:233 678:20 ...
            tokens = {}                        
            testFile.write("{} ".format(classSymbol))
            
            for line in inputFile:
                features = line.split()
                
                for feature in features:
                    extractedFeatures = getFeatures(feature.strip()) #getFeatures(feature.strip()) #[feature.strip()]
                    
                    for token in extractedFeatures:
                        indexInVocab = -1
                        
                        try:
                            indexInVocab = vocabulary[token]
                        except KeyError:
                            continue #for the moment, ignoring unknown words                           
                            '''vocabulary[token] = vocabularyID
                            vocabularyID += 1
                            indexInVocab = vocabulary[token]'''                         
                        
                        try:
                            tokens[indexInVocab] += 1
                        except KeyError:
                            tokens[indexInVocab] = 1
                      
            #print the line in training set corresponding to this example
            for tokenNumber in sorted(tokens.keys()):
                testFile.write("{}:{} ".format(tokenNumber, tokens[tokenNumber]))            
            
            #leave a line after the current file has been represented  
            testFile.write("\n")               
            
            inputFile.close()  
        
        testFile.close()
        
    except FileNotFoundError as e:
        print("Directory not found. Error: "+str(e))
    except BaseException as e:
        testFile.close()
        inputFile.close()
        print("Unexpected Error occurred: "+str(e)+". File in process: "+str(directoryName+"/"+fileName))
        
def getLabel(fileName):
    return fileName.split('.')[0];

def main():
    numOfArgs = len(sys.argv) - 1; #1st argument is always the source file name.
    if(numOfArgs < 2):
        print("Argument missing. Correct syntax is \n python3 svmTestDataFormatter.py <inputDirectory> <formattedTestSet>")    
    elif(numOfArgs > 2):
        print("Too many arguments. Correct syntax is \n python3 svmTestDataFormatter.py <inputDirectory> <formattedTestSet>")
    else:
        directoryName = sys.argv[1];
        formattedTrainingSet = sys.argv[2]
        formatTestData(directoryName, formattedTrainingSet);

if __name__ == '__main__': main()