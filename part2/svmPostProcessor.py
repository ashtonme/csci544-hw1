'''
Created on Jan 30, 2015

@author: ashton
'''

import sys

def postProcessOutput(megaMOutputFileName):
    try:
        svmOutput = open(megaMOutputFileName, mode='r')
       
        #what symbol to use for ham and what for spam
        try:
            classSymbolsFile = open("svmClassSymbols.txt", mode='r')
        except FileNotFoundError as e:
            print("svmClassSymbols.txt not found. Did you delete it? Run svmTrainingDataFormatter.py again. {}".format(e))
            return
        
        classSymbols = {}
        lineNumber = 0
        for line in classSymbolsFile:
            lineNumber += 1
                        
            if(lineNumber > 2):
                print("SVM light is not designed for classification involving more than 2 classSymbols.")
                return
            
            classSymbol = int(line.split()[1].strip()) #+1 or -1
            className = line.split()[0].strip() #ham or spam
            
            classSymbols[classSymbol] = className
        classSymbolsFile.close()               
        
        #convert predictions to class symbols
        for line in svmOutput:
            prediction = float(line)
            if(prediction <= 0.0):
                print("{}".format(classSymbols[-1]))
            else:
                print("{}".format(classSymbols[1]))
                
        svmOutput.close()        
    except FileNotFoundError as e:
        print("File not found. Error: "+str(e))
    except BaseException as e:
        print("Unexpected Error occurred: "+str(e))

def main():
    numOfArgs = len(sys.argv) - 1; #1st argument is always the source file name.
    if(numOfArgs < 1):
        print("Argument missing. Correct syntax is \n python3 svmPostProcessor.py <svmOutput>")    
    elif(numOfArgs > 1):
        print("Too many arguments. Correct syntax is \n python3 svmPostProcessor.py <svmOutput>")
    else:
        svmOutputFileName = sys.argv[1];
        postProcessOutput(svmOutputFileName)

if __name__ == '__main__' : main()