# README


## Information about part 1 and part 2

### For part 1: (Naive Bayes Classifier - Spam Detection and Sentiment Analysis)

1. _python3 trainingDataFormatter.py <path to directory containing training examples> > nbTrainingData_
	* Reads the files from the directory specified and formats them according to the specs provided in the HW1 description. Please note that trainingDataFormatter.py will accept a DIRECTORY only and not a single file. It writes to stdout, from where it is written to 'nbTrainingData'.
	
2. _python3 nblearn.py nbTrainingData spam.nb OR python3 nblearn.py nbTrainingData sentiment.nb_
	* Reads the formatted input provided by trainingDataFormatter.py and creates a model called 'spam.nb' for classification.

3. _python3 testDataFormatter.py <path to directory containing test data> > nbTestData_
	* Reads the files contained in the directory specified and formats them according to the specs provided in HW1 for test data. 'nbTestData' is the formatted test data. Please note that testDataFormatter.py will accept a DIRECTORY only and not a single file.

4. _python3 nbclassify.py spam.nb nbTestData > spam.out OR python3 nbclassify.py sentiment.nb nbTestData > sentiment.out_
	* Uses the model provided by nblearn.py and uses the model to classify tests contained in 'nbTestData'. Writes the output to spam.out (it contains predicted class labels in the same order as the files in the directory specified to testDataFormatter.py).


If you wish to use it, all the above 4 commands are contained in an executable script file named script_nbSpamClassifier and script_nbSentimentClassifier. All you have to do is replace the placeholders with the appropriate paths to directories.






### For part 2: (Text classification using SVM and MegaM):

#### For SVM:

1. _python3 svmTrainingDataFormatter.py <path to directory containing training examples> svmTrainingData_
	* Reads the files contained in the directory specified and formats them according to the specifications required by SVM light. The output will be fed to svm_learn for creating a model for classification.

2. _./svm_learn svmTrainingData spam.svm.model OR ./svm_learn svmTrainingData sentiment.svm.model_
	* Reads the file containing the formatted training set and creates a model 'spam.svm.model' based on this.

3. _python3 svmTestDataFormatter.py <path to directory containing test data> svmTestData_
	* Formats the test data depending on the specs provided in HW1, which will be fed to the classifier.

4. _./svm_classify svmTestData spam.svm.model svmResults.txt OR ./svm_classify svmTestData sentiment.svm.model svmResults.txt_
	* Invokes the svm classifier, providing it with the model created in step 2 and the formatted test data created in step 3.

5. _python3 svmPostProcessor.py svmResults.txt > spam.svm.out OR python3 svmPostProcessor.py svmResults.txt > sentiment.svm.out_
	* The output of svm_classify needs to be post processed in order to match the specs of HW1. svmPostProcessor does just that and provides the proper format in spam.svm.out


If you wish to use it, all the above 5 commands are contained in the folder 'part2', in an executable script file named script_svmSpamClassifier and script_svmSentimentClassifier. All you have to do is replace the placeholders with the appropriate paths to directories.




#### For MegaM:

1. _python3 megaMTrainingDataFormatter.py <path to folder containing training data> -dev <path to folder containing development data>  >  megaMtrainingData_

	* Reads the files from the directory specified and formats the data according to the specs of HW1.

2. _./megam_i686.opt -nc -fvals binary megaMtrainingData > spam.megam.model OR ./megam_i686.opt -nc -fvals binary megaMtrainingData > sentiment.megam.model_

	* Reads the formatted input file provided by megaMTrainingDataFormatter.py and creates the model.

3. _python3 megaMTestDataFormatter.py <path to folder containing test data> > megaMtestData_

	* Reads the files to be used for testing from the directory specified and formats them into a single file according to the specs of HW1.

4. _./megam_i686.opt -nc -fvals -predict spam.megam.model binary megaMtestData > megaMResults.txt_

	* Performs classification based on the model created in step 2 and the formatted test data provided in step 3.

5. _python3 megaMPostProcessor.py megaMResults.txt > spam.megam.out_

	* Post processes the output of the megaM classifier, basically just replaces the class symbols (required for megaM) with the actual class names.


If you wish to use it, all the above 5 commands are contained in the folder 'part2', in an executable script file named script_megaMSpamClassifier and script_megaMSentimentClassifier. All you have to do is replace the placeholders with the appropriate paths to directories.





## Performance measures

### Naive Bayes classifier:

:::ON DEVELOPMENT DATA:::

1.  SPAM detection:
	
	Precision for HAM: 0.9919354838709677
	Precision for SPAM: 0.9568733153638814

	Recall for HAM: 0.984
	Recall for SPAM: 0.977961432506887

	F-Score for HAM: 0.9879518072289156
	F-Score for SPAM: 0.9673024523160764

	Percentage of correct classifications: 98.23917828319883 %
	
	
	
2. Sentiment Analysis: (Randomly moved 30% of files from SPAM_training into a new folder called SPAM_dev)

	Precision for POS: 0.8724754760530872
	Precision for NEG: 0.8158155676747645

	Recall for POS: 0.8027608176267587
	Recall for NEG: 0.8027608176267587

	F-Score for POS: 0.8361675653255911
	F-Score for NEG: 0.8474314407106991
	
	Percentage of correct classifications: 84.2 %


### Binary classification using SVM:

:::ON DEVELOPMENT DATA:::

1.  SPAM detection:

	Precision for HAM: 0.953757225433526
	Precision for SPAM: 0.9692307692307692

	Recall for HAM: 0.99
	Recall for SPAM: 0.8677685950413223

	F-Score for HAM: 0.971540726202159
	F-Score for SPAM: 0.9156976744186046
	
	Percentage of correct classifications: 95.74468085106383 %
	
	
	
2. Sentiment Analysis: (Randomly moved 30% of files from SPAM_training into a new folder called SPAM_dev)

	Precision for POS: 0.8627654065251166
	Precision for NEG: 0.8804288070368335

	Recall for POS: 0.8845234934961508
	Recall for NEG: 0.8580230377712296

	F-Score for POS: 0.8735089788963166
	F-Score for NEG: 0.8690815357482025
	
	Percentage of correct classifications: 87.13333333333333 %
	
	

### Classification using MegaM:

:::ON DEVELOPMENT DATA:::

1.  SPAM detection:

	Precision for HAM: 0.990990990990991
	Precision for SPAM: 0.9725274725274725

	Recall for HAM: 0.99
	Recall for SPAM: 0.9752066115702479

	F-Score for HAM: 0.9904952476238119
	F-Score for SPAM: 0.9738651994497937
	
	Percentage of correct classifications: 98.60601614086573 %
	
	
	
2. Sentiment Analysis: (Randomly moved 30% of files from SPAM_training into a new folder called SPAM_dev)

	Precision for POS: 0.8762559492332099
	Precision for NEG: 0.8781603012372243

	Recall for POS: 0.8797451552959915
	Recall for NEG: 0.8746316635413877

	F-Score for POS: 0.8779970857067161
	F-Score for NEG: 0.8763924305462355

	Percentage of correct classifications: 87.72 %
	
	

## When only 10% of the training data is used to train the classifiers

Let us consider the Naive Bayes classifier.
According to me, when only 10% of the training data is used to train the classifier, chances are high that the vocabulary of the model will have fewer tokens compared to the model constructed using 100% of the training data.
As a result, when test data is encountered, the number of unknown tokens increases and hence the P(Msg | class) reduces to a great extent. Hence, the P(class | Msg) will depend on the prior probabilities, 
leading to a reduction in accuracy.

### Naive Bayes classifier:

:::ON DEVELOPMENT DATA:::

1.  SPAM detection:

	Precision for HAM: 0.985929648241206
	Precision for SPAM: 0.9483695652173914

	Recall for HAM: 0.981
	Recall for SPAM: 0.9614325068870524

	F-Score for HAM: 0.9834586466165413
	F-Score for SPAM: 0.9548563611491109
	
	Percentage of correct classifications: 97.57887013939839 %
	
	
	
2. Sentiment Analysis: (Randomly moved 30% of files from SPAM_training into a new folder called SPAM_dev)

	Precision for POS: 0.8450620934358368
	Precision for NEG: 0.7792617775619233

	Recall for POS: 0.7586939208919564
	Recall for NEG: 0.8596303241360835

	F-Score for POS: 0.7995523849489438
	F-Score for NEG: 0.8174754808304675
	
	Percentage of correct classifications: 80.89333333333333 %
	
	

### Binary classification using SVM:

:::ON DEVELOPMENT DATA:::

1.  SPAM detection:

	Precision for HAM: 0.8735529830810329
	Precision for SPAM: 0.9208333333333333

	Recall for HAM: 0.981
	Recall for SPAM: 0.6088154269972452

	F-Score for HAM: 0.9241639189825718
	F-Score for SPAM: 0.7330016583747927
	
	Percentage of correct classifications: 88.18782098312545 %
	
	
	
2. Sentiment Analysis: (Randomly moved 30% of files from SPAM_training into a new folder called SPAM_dev)

	Precision for POS: 0.7824192721681189
	Precision for NEG: 0.8015564202334631

	Recall for POS: 0.810459251393682
	Recall for NEG: 0.786795798663211

	F-Score for POS: 0.7961924631633851
	F-Score for NEG: 0.8690815357482025
	
	Percentage of correct classifications: 79.16 %
	



### Classification using MegaM:

:::ON DEVELOPMENT DATA:::

1.  SPAM detection:

	Precision for HAM: 0.9619512195121951
	Precision for SPAM: 0.9585798816568047

	Recall for HAM: 0.986
	Recall for SPAM: 0.8925619834710744

	F-Score for HAM: 0.9738271604938271
	F-Score for SPAM: 0.9243937232524964
	
	Percentage of correct classifications: 96.11151870873074 %
	
	
	
2. Sentiment Analysis: (Randomly moved 30% of files from SPAM_training into a new folder called SPAM_dev)

	Precision for POS: 0.8164638881698162
	Precision for NEG: 0.831454495463294

	Recall for POS: 0.8372710379612424
	Recall for NEG: 0.8100723278864185

	F-Score for POS: 0.8267365661861074
	F-Score for NEG: 0.8206241519674355

	Percentage of correct classifications: 82.37333333333333 %